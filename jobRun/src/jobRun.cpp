#include "yaml-cpp/yaml.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fstream>

using namespace std;

struct Job
{
          std::string Name;
          std::string Exec;
          std::vector<string> Args;
          std::string Input;
          std::string Output;
          std::string Error;
};

void RunExec(const char* exec, char** argNode, string input, string output, string error){

          pid_t child_pid;
          int child_status;
          child_pid = fork();

          if(child_pid==0){

                    std::cout << "...Proceso Hijo..." << endl;

                    if(input=="stdin" && output=="stdout"){

                              try{
                                        std::cout<< "## Letura y escritura en consola##" <<endl;
                                        execvp(exec,argNode);
                                        std::cout<< "## finished succesfully ##" <<endl;
                              }catch (int e){
                                        std::cout << "An error occurred: " << e << '\n';
                                        std::cout << error<<endl;
                              }
                    }else{
                              if(input=="stdin" && output!="stdout"){
                                        try{
                                                  std::cout<< "## Lectura en consola y escritura en file##" <<endl;
                                                  execvp(exec,argNode);
                                                  ofstream myfile;
                                                  myfile.open ("output.txt");myfile << "Finished succesfully.\n";
                                                  myfile.close();
                                        }catch (int e){
                                                  cout << "An error occurred: "<< e << '\n';
                                                  std::cout << error<<endl;
                                        }
                              }else{
                                        if(input!="stdin" && output=="stdout"){
                                        }else{
                                                  if(input!="stdin" && output!=="stdout"){}
                              }
                    }
          }
          exit(0);
          }else {
                    wait(&child_status);
          }
}


int main(int argc, char *argv[]){

          YAML::Node node = YAML::LoadFile(argv[1]);
          Job j;
          int size = node[1]["Args"].size();
          char* argNode[size];
          for(int i=0;i<size;i++){
                    argNode[i] = const_cast<char*>(node[1]["Args"][i].as<string>().c_str());
          }
          argNode[size+1] = NULL;
          j.Name = node[1]["Name"].as<std::string>();
          j.Exec = node[1]["Exec"].as<std::string>();
          j.Input =node[1]["Input"].as<std::string>();
          j.Output = node[1]["Output"].as<std::string>();
          j.Error = node[1]["Error"].as<std::string>();
          std::cout << "## Running " << j.Name.c_str() << " ##" << endl;

          RunExec(j.Exec.c_str(),argNode, j.Input.c_str(), j.Output.c_str(), j.Error.c_str());
          std::cout << "## "<< " finished succesfully ##" << endl;
}