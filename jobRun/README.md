# README #

Autores:

- Daniela Barrientos Grisales
- Alejandro Alvarez Cadavid

Descripción:

JobRun es un programa escrito en C++, que permite a partir de la lectura
de un archivo de extensión .yaml ó .yml ejecutar llamadas al sistema
de tal forma que el usuario pueda modificar los parametros de entrada
dentro del archivo y el programa realiza las llamadas respectivas mediante
las instrucciones de execvp. Este programa corre sobre una plataforma
Linux.

***Versión del SO con el que se probó este proyecto:
Fedora 21

***Versión de la librería:
yaml-cpp-0.5.1
Desde: https://code.google.com/p/yaml-cpp/downloads/detail?name=yaml-cpp-0.5.1.tar.gz&can=2&q=

***Adicionales:
CMake,
Libboost-all-dev

***Cómo compilar:
//En una misma carpeta se debe tener el .yaml y el .cpp
//Desde la terminal me debo parar en dicha carpeta, y luego escribir:
g++ jobRun.cpp -o jobRun -L/usr/local/lib -I/usr/local/include -lyaml-cpp


***Referencias de código:

//Ejemplo del uso del fork:
http://www.cs.ecu.edu/karl/4630/sum01/example1.html

//Ejemplos de yaml:
https://code.google.com/p/yaml-cpp/wiki/Tutorial

//Esperar a que un proceso termine:
https://support.sas.com/documentation/onlinedoc/sasc/doc/lr2/wait.htm

//Execvp
http://linux.die.net/man/3/execvp

//******************Configuración de la librería de Yaml:
//DBG

//Entro a la carpeta en que tengo la librería:
cd Downloads/yaml-cpp-0.5.1
cd build

//Luego entro a el perfil de administrador en Fedora:
su -

//Luego:
make install

//Reviso qué hay dentro, debería aparecer cmake
ls

//Me salgo:
cd..

//entro a home
cd home

//reviso qué hay ahí, debería aparecer la carpeta de usuario
ls

//Entro a la carpeta de usuario
cd liveuser//En este caso
cd Downloads
cd yaml-cpp-0.5.1
cd build
make install


//**********Instalando libboost
su -
yum install libboost-all-dev


//**********Instalando g++
su -
yum install libboost-all-dev

//************Instalando cmake
yum install cmake

//**************Corriendo el programa:
g++ JobRun.cpp -o JobRun -L/usr/local/lib -I/usr/local/include -lyaml-cpp

//Al completar la instruccion anterior solo es cuestion de realizar el siguiente
//comando para correr la aplicación

./JobRun <archivo YAML>
